namespace HVR
{
    public static class VersionInfo
    {
        public static string BUILD_NUMBER = "115";
        public static string BUILD_HOST = "WindowsNVidiaBenchmark1";
        public static string BUILD_DATE = "2018-09-26 14:48";
        public static string BUILD_INFO = "WindowsNVidiaBenchmark1 2018-09-26 14:48";
        public static string GIT_BRANCH = "release/0.6.0f1";
        public static string GIT_HASH = "c7fc0d310bcfc7975966bc164cfff9970fd7574c";
        public static string GIT_MODIFIED = "False";
        public static string GIT_INFO = "release/0.6.0f1 c7fc0d310bcfc7975966bc164cfff9970fd7574c";
        public static string VERSION = "0.6.0f1";
    }
}
